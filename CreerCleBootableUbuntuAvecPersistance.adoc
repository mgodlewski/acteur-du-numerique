= Création de clé bootable Ubuntu avec persistance

* Télécharger la dernière iso Ubuntu Desktop
* A partir d'un poste linux, installer l'utilitaire link:https://help.ubuntu.com/community/mkusb[mkusb]
* <Installation avec Persistance>
** d:  dus , guidus, mkusb-dus    - Classic, easy to use
** i:  Install (make a boot device)
** p: 'Persistent live' - only Debian and Ubuntu then select the iso file
** Select target device
** Do you really want to overwrite this target device? Yes
** Persistent live drive settings : msdos
** Select space for persistence (percent): 70
** Select version of usb-pack
** Final checkpoint, go ahead? Go!
* Booter sur la clé et réaliser la configuration de
** disposition clavier
** Fuseau horaire
** Répérer comment configurer la souris pour les gauchers
** Installer GCompris
** Suppression des raccourcis applicatifs par défaut (amazon, thunderbird, ...). Je n'ai gardé que LibreOffice et ai ajouté GCompris

