== Meet Edison : Rencontre avec le robot Edison

=== Séance préparatoire

Sans montrer les robots, poser aux élèves la question "qu'est-ce qu'un robot?" et recueillir les réponses sur une feuille. (5 min)

Donner à chacun une feuille et, à l'aide de feutres ou de crayon de couleur, inviter les élèves a dessiner un robot. (15 min)

Afficher les dessins des robots au tableau et recueillir les conceptions suivantes sous forme de tableau (formes, utilité, moyens de locomotion) (10 min)
* Comment sont vos robots ? (forme, couleur …)
* A quoi servent-ils ? (guerre, nettoyage, danse …)
* Comment avancent vos robots ? (jambes, roues, ailes …)

Le link:https://fr.wikipedia.org/wiki/Robot[robot] est conçu pour accomplir automatiquement des tâches imitant ou reproduisant des actions humaines.

Données en entrée : capteurs => Traitement => Données en sortie : mouvement, son, lumière

image::https://upload.wikimedia.org/wikipedia/commons/9/92/CPT_Hardware-InputOutput.svg[InputProcessOutput, 500]

=== Séance 2

Préparer divers endroits au sol (aller dans une grande salle au besoin):
* Ligne noire (scotch electricien) au sol ou sur un tapis de gym
* Boite Lego
* Obstacles
* Balle en mousse
* Télécommande

Donner à chaque élève un robot éteint. Proposer leur de faire connaissance avec leur robot. Les laisser en autonomie. (5 min)

Mise en commun : Les élèves expliquent comment ils ont fait pour l’allumer, s’ils ont entendu des sons ou pas, qu’il y a des boutons de différentes couleurs, qu’il y a des roues, et que le robot avance. (5 min)

Présentation de tous les boutons du robot (5 min) (utiliser les cartes de représentations des boutons à la page suivante) et démonstration au sol.
Laisser les enfants s’entraîner avec leur robot en groupe.(5 min)

Demander aux enfants de dessiner le robot. (peut être fait à une autre séance)


Inspiration :
* http://www.enmaternelle.fr/2017/01/19/decouverte-des-robots-blue-bots/

